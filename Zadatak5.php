<?php

    // 5. Napisati funkciju koja prima kao parametar neki broj $n, i vraća dvodimenzionalni niz dimenzija NxN, popunjen nulama. Na primer, ako joj se prosledi broj 3, funkcija treba da vrati sledeći niz:
    //
    // [
    //     [0, 0, 0],
    //     [0, 0, 0],
    //     [0, 0, 0]
    // ]

    function napraviNizNxN($n)
    {
        $niz = [];
        for ($i = 0; $i < $n; $i++) {
            $niz[] = [];
            for ($j = 0; $j < $n; $j++) {
                $niz[$i][] = 0;
            }
        }
        return $niz;
    }

?>
