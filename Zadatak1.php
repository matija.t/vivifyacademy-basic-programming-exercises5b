<?php

// 1. Napisati funkciju koja prima višedimenzionalni niz kao parametar i vraća niz sa svim elementima tog niza, koji nisu i sami nizovi. Primer:
//
// Ako se prosledi niz        [1, 2, [3, 4], 5, 6, 7, [8], 9]
// Funkcija treba da vrati    [1, 2, 5, 6, 7, 9]
//
// Da li je neka vrednost niz ili ne, može se proveriti pomoću ugrađene funkcije is_array().

    function sveOsimNizova($originalniNiz)
    {
        $noviNiz = [];
        foreach ($originalniNiz as $element) {
            if (!is_array($element)) {
                $noviNiz[] = $element;
            }
        }
        return $noviNiz;
    }


?>
