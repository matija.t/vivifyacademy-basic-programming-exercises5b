<?php

    // 4. Napisati verziju funkcije iz prethodnog zadatka koja računa zbir u višedimenzionalnom nizu gde su niz i podnizovi proizvoljne dužine:
    //
    // [
    //     [10, 14, 6, 1, 0, 24],
    //     [4, 12, 2, 50],
    //     [21, 8, 17],
    //     [6, 9, 15, 11, 31]
    // ]


    function zbirDvodimenzionalnogNxM($niz)
    {
        $zbir = 0;
        foreach ($niz as $podniz) {
            foreach ($podniz as $element) {
                $zbir += $element;
            }
        }
        return $zbir;
    }

    // Ovo resenje radi i za prethodni zadatak

?>
