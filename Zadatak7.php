<?php

    // 7. Napisati drugu verziju funkcije iz prethodnog zadatka, koja radi i sa kretanjem van granica dvodimenzionalnog niza (npr. ako smo trenutno na levoj ivici i sledeći korak je 'left', prebaciti poziciju na suprotnu stranu, tako da smo sada na desnoj ivici).

    function kretanjeKrozNizBezIzlaska($niz2D, $koraci)
    {
        $x = 0;
        $y = 0;
        foreach ($koraci as $korak) {

            if ($korak == 'right') {
                $x++;

                if ($x > count($niz2D[$y]) - 1) {
                    $x = 0;
                }
            } else if ($korak == 'left') {
                $x--;

                if ($x < 0) {
                    $x = count($niz2D[$y]) - 1;
                }
            } else if ($korak == 'down') {
                $y++;

                if ($y > count($niz2D) - 1) {
                    $y = 0;
                }
            } else if ($korak == 'up') {
                $y--;

                if ($y < 0) {
                    $y = count($niz2D) - 1;
                }
            }
        }
        return $niz2D[$y][$x];
    }


?>
